from django import forms


class FormEstudiante(forms.Form):
   nombre = forms.CharField(label="Nombre", help_text="Nombre del Estudiante.")