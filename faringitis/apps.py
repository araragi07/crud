from django.apps import AppConfig


class FaringitisConfig(AppConfig):
    name = 'faringitis'
