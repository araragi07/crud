from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from django.urls import reverse
from faringitis.forms import FormEstudiante
from faringitis.models import Estudiante


def formulario_estudiante(request):
   if request.method == 'POST':
       form = FormEstudiante(request.POST)
       if form.is_valid():
           nombre = request.POST.get("nombre")
           miEstudiante = Estudiante.objects.create(nombre=nombre)
           miEstudiante.save()
           return HttpResponse("Estudiante creado con éxito")
   else:
       form = FormEstudiante()
   context = {
       'form': form,
   }
   return render(request, 'crearEstudiante.html', context)